package com.project.bli.service;

import java.util.List;
import java.util.Optional;

import com.project.bli.entity.Shuttle;

public interface ShuttleService {

	List<Shuttle> findAllShuttle();
	Optional<Shuttle> findById(Integer id);
	Optional<Shuttle> findByName(String name);
	List<Shuttle> findBySizeCategory(String sizeCategory);
	
	boolean createShuttle(Shuttle shuttle);
	boolean updateShuttle(Shuttle shuttle);
	boolean deleteShuttle(Integer id);
	
}

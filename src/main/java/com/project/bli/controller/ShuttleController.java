package com.project.bli.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.bli.entity.Shuttle;
import com.project.bli.service.impl.ShuttleServiceImpl;

@RestController
@RequestMapping("/api/shuttle/shuttle")
public class ShuttleController {

	private static final Logger logger = LoggerFactory.getLogger(ShuttleController.class);
	
	@Autowired
	private ShuttleServiceImpl shuttleService;
	
	@GetMapping
	public ResponseEntity<List<Shuttle>> findAllShuttle() {
		logger.info("[CONTROLLER] Find all shuttle controller triggered!");
		List<Shuttle> shuttles = shuttleService.findAllShuttle();
		return new ResponseEntity<List<Shuttle>>(shuttles, HttpStatus.OK);
	}
	
	@GetMapping("/id/{id}")
	public ResponseEntity<Optional<Shuttle>> findShuttleById(@PathVariable Integer id) {
		logger.info("[CONTROLLER] Find shuttle by ID controller triggered!");
		Optional<Shuttle> shuttle = shuttleService.findById(id);
		return new ResponseEntity<Optional<Shuttle>>(shuttle, HttpStatus.OK);
	}
	
	@GetMapping("/name/{name}")
	public ResponseEntity<Optional<Shuttle>> findShuttleByName(@PathVariable String name) {
		logger.info("[CONTROLLER] Find shuttle by name controller triggered!");
		Optional<Shuttle> shuttle = shuttleService.findByName(name);
		return new ResponseEntity<Optional<Shuttle>>(shuttle, HttpStatus.OK);
	}
	
	@GetMapping("/size/{size}")
	public ResponseEntity<List<Shuttle>> findBySizeCategory(@PathVariable String size) {
		logger.info("[CONTROLLER] Find shuttle by ID controller triggered!");
		List<Shuttle> shuttles = shuttleService.findBySizeCategory(size);
		return new ResponseEntity<List<Shuttle>>(shuttles, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Void> createShuttle(@RequestBody Shuttle shuttle) {
		logger.info("[CONTROLLER] Create shuttle controller triggered!");
		boolean flag = shuttleService.createShuttle(shuttle);
		
		if (flag) {
			logger.info("[CONTROLLER] Successfully created new Shuttle ...");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		logger.info("[CONTROLLER] Failed to create new Shuttle ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
	@PutMapping
	public ResponseEntity<Void> updateShuttle(@RequestBody Shuttle shuttle) {
		logger.info("[CONTROLLER] Delete shuttle controller triggered!");
		boolean flag = shuttleService.updateShuttle(shuttle);
		
		if (flag) {
			logger.info("[CONTROLLER] Successfully updated new Shuttle ...");
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		logger.info("[CONTROLLER] Failed to update new Shuttle ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> deleteShuttle(@PathVariable Integer id) {
		logger.info("[CONTROLLER] Delete shuttle controller triggered!");
		boolean flag = shuttleService.deleteShuttle(id);
		
		if (flag) {
			logger.info("[CONTROLLER] Successfully deleted existing Shuttle ...");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		logger.info("[CONTROLLER] Failed to delete existing Shuttle ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
}

package com.project.bli.dao.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.project.bli.dao.ShuttleDAO;
import com.project.bli.entity.MeetingPoint;
import com.project.bli.entity.Shuttle;
import com.project.bli.repository.MeetingPointRepository;
import com.project.bli.repository.ShuttleRepository;

@Repository
@Transactional
public class ShuttleDAOImpl implements ShuttleDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(ShuttleDAOImpl.class);
	
	@Autowired
	private ShuttleRepository shuttleRepository;
	
	@Autowired
	private MeetingPointRepository meetingPointRepository;

	@Override
	public List<Shuttle> findAllShuttle() {
		logger.info("[DAO] Find all shuttle DAO triggered!");
		return shuttleRepository.findAll();
	}

	@Override
	public Optional<Shuttle> findById(Integer id) {
		logger.info("[DAO] Find shuttle by ID DAO triggered!");
		return shuttleRepository.findById(id);
	}

	@Override
	public Optional<Shuttle> findByName(String name) {
		logger.info("[DAO] Find shuttle by name DAO triggered!");
		return shuttleRepository.findByName(name);
	}

	@Override
	public List<Shuttle> findBySizeCategory(String sizeCategory) {
		logger.info("[DAO] Find shuttle by size category DAO triggered!");
		return shuttleRepository.findBySizeCategory(sizeCategory);
	}

	@Override
	public boolean createShuttle(Shuttle shuttle) {
		logger.info("[DAO] Create shuttle DAO triggered!");
		
		// Check if Meeting Point ID exists
		Optional<MeetingPoint> meetingPoint = meetingPointRepository.findById(shuttle.getMeetingPoint().getId());
		if (!meetingPoint.isPresent()) {
			logger.info("[DAO] Meeting point ID doesn't exists ...");
			return false;
		}
		logger.info("[DAO] Meeting point exists ...");
		
		// Check if shuttle name already exists
//		logger.info("[DAO] Checking if shuttle already exists ...");
//		Optional<Shuttle> checker = shuttleRepository.findByName(shuttle.getName());
//		if (checker.isPresent()) {
//			logger.info("[DAO] Shuttle with name: " + shuttle.getName() + " already present ...");
//			return false;
//		}
		
		// Attempting to create shuttle
		logger.info("[DAO] Shuttle with name: " + shuttle.getName() + " is available ...");
		logger.info("Attempting to create new shuttle ...");
		shuttleRepository.save(shuttle);
		
		logger.info("[DAO] Create shuttle potentially successful ...");
		return true;
	}

	@Override
	public boolean updateShuttle(Shuttle shuttle) {
		logger.info("[DAO] Update shuttle DAO triggered!");
		
		// Check if Meeting Point ID exists
		Optional<MeetingPoint> meetingPoint = meetingPointRepository.findById(shuttle.getMeetingPoint().getId());
		if (!meetingPoint.isPresent()) {
			logger.info("[DAO] Meeting point ID doesn't exists ...");
			return false;
		}
		logger.info("[DAO] Meeting point exists ...");
		
		// Check if shuttle exists
		logger.info("[DAO] Checking if shuttle exists ...");
		Optional<Shuttle> checker = shuttleRepository.findById(shuttle.getId());
		
		if (!checker.isPresent()) {
			logger.info("[DAO] Shuttle doesn't exists ...");
			return false;
		}
		logger.info("[DAO] Shuttle potentially exists ...");
		
		// Attempting to update shuttle
		logger.info("[DAO] Attempting to update shuttle ...");
		shuttleRepository.save(shuttle);
		
		logger.info("[DAO] Update shuttle potentially successful ...");
		return true;
	}

	@Override
	public boolean deleteShuttle(Integer id) {
		logger.info("[DAO] Delete shuttle DAO triggered!");
		logger.info("[DAO] Attempting to delete shuttle ...");
		shuttleRepository.deleteById(id);
		return true;
	}

}

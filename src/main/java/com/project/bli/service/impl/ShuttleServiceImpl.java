package com.project.bli.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.bli.dao.impl.ShuttleDAOImpl;
import com.project.bli.entity.Shuttle;
import com.project.bli.service.ShuttleService;

@Service
public class ShuttleServiceImpl implements ShuttleService {
	
	private static final Logger logger = LoggerFactory.getLogger(ShuttleServiceImpl.class);
	
	@Autowired
	private ShuttleDAOImpl shuttleDAOImpl;

	@Override
	public List<Shuttle> findAllShuttle() {
		logger.info("[SERVICE] Find all shuttle service triggered!");
		return shuttleDAOImpl.findAllShuttle();
	}

	@Override
	public Optional<Shuttle> findById(Integer id) {
		logger.info("[SERVICE] Find shuttle by ID service triggered!");
		return shuttleDAOImpl.findById(id);
	}

	@Override
	public Optional<Shuttle> findByName(String name) {
		logger.info("[SERVICE] Find shuttle by name service triggered!");
		return shuttleDAOImpl.findByName(name);
	}

	@Override
	public List<Shuttle> findBySizeCategory(String sizeCategory) {
		logger.info("[SERVICE] Find shuttle by size category service triggered!");
		return shuttleDAOImpl.findBySizeCategory(sizeCategory);
	}

	@Override
	public boolean createShuttle(Shuttle shuttle) {
		logger.info("[SERVICE] Create shuttle service triggered!");
		return shuttleDAOImpl.createShuttle(shuttle);
	}

	@Override
	public boolean updateShuttle(Shuttle shuttle) {
		logger.info("[SERVICE] Update service triggered!");
		return shuttleDAOImpl.updateShuttle(shuttle);
	}

	@Override
	public boolean deleteShuttle(Integer id) {
		logger.info("[SERVICE] Delete shuttle service triggered!");
		return shuttleDAOImpl.deleteShuttle(id);
	}

}

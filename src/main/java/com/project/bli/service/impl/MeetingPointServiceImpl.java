package com.project.bli.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.bli.dao.impl.MeetingPointDAOImpl;
import com.project.bli.entity.MeetingPoint;
import com.project.bli.service.MeetingPointService;

@Service
public class MeetingPointServiceImpl implements MeetingPointService {

	private static final Logger logger = LoggerFactory.getLogger(MeetingPointServiceImpl.class);
	
	@Autowired
	private MeetingPointDAOImpl meetingPointDAOImpl;
	
	@Override
	public List<MeetingPoint> findAllMeetingPoint() {
		logger.info("[SERVICE] Find all meeting point service triggered!");
		return meetingPointDAOImpl.findAllMeetingPoint();
	}

	@Override
	public Optional<MeetingPoint> findByMeetingPointName(String name) {
		logger.info("[SERVICE] Find meeting point by name service triggered!");
		return meetingPointDAOImpl.findByMeetingPointName(name);
	}

	@Override
	public List<MeetingPoint> findByTimeToBli(Date timeToBli) {
		logger.info("[SERVICE] Find meeting point by 'time to bli' service triggered!");
		return meetingPointDAOImpl.findByTimeToBli(timeToBli);
	}

	@Override
	public List<MeetingPoint> findByTimeFromBli(Date timeFromBli) {
		logger.info("[SERVICE] Find meeting point by 'time from bli' service triggered!");
		return meetingPointDAOImpl.findByTimeFromBli(timeFromBli);
	}

	@Override
	public Optional<MeetingPoint> findById(Integer id) {
		logger.info("[SERVICE] Find meeting point by ID service triggered!");
		return meetingPointDAOImpl.findById(id);
	}

	@Override
	public boolean createMeetingPoint(MeetingPoint meetingPoint) {
		logger.info("[SERVICE] Create new Meeting Point service triggered!");
		return meetingPointDAOImpl.createMeetingPoint(meetingPoint);
	}

	@Override
	public boolean updateMeetingPoint(MeetingPoint meetingPoint) {
		logger.info("[SERVICE] Update Meeting Point service triggered!");
		return meetingPointDAOImpl.updateMeetingPoint(meetingPoint);
	}

	@Override
	public boolean deleteMeetingPoint(Integer id) {
		logger.info("[SERVICE] Delete Meeting Point service triggered!");
		return meetingPointDAOImpl.deleteMeetingPoint(id);
	}

}

package com.project.bli.dao.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.project.bli.dao.MeetingPointDAO;
import com.project.bli.entity.MeetingPoint;
import com.project.bli.repository.MeetingPointRepository;

@Repository
@Transactional
public class MeetingPointDAOImpl implements MeetingPointDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(MeetingPointDAOImpl.class);
	
	@Autowired
	private MeetingPointRepository meetingPointRepository;

	@Override
	public List<MeetingPoint> findAllMeetingPoint() {
		logger.info("Attempting to return all meeting point(s) ...");
		return meetingPointRepository.findAll();
	}
	
	@Override
	public Optional<MeetingPoint> findByMeetingPointName(String name) {
		logger.info("Attempting to return meeting point by name ...");
		return meetingPointRepository.findByMeetingPointName(name);
	}

	@Override
	public List<MeetingPoint> findByTimeToBli(Date timeToBli) {
		logger.info("Attempting to return meeting point with 'timeToBli' ...");
		return meetingPointRepository.findByTimeToBli(timeToBli);
	}

	@Override
	public List<MeetingPoint> findByTimeFromBli(Date timeFromBli) {
		logger.info("Attempting to return meeting point with 'timeFromBli' ...");
		return meetingPointRepository.findByTimeFromBli(timeFromBli);
	}

	@Override
	public Optional<MeetingPoint> findById(Integer id) {
		logger.info("Attempting to return meeting point with ID: " + id + " ...");
		return meetingPointRepository.findById(id);
	}

	@Override
	public boolean createMeetingPoint(MeetingPoint meetingPoint) {
		logger.info("[DAO] Create Meeting Point DAO triggered!");
		
		// Check if meeting point name already exists
		logger.info("[DAO] Check if meeting point with name: " + meetingPoint.getMeetingPointName() + " exists ...");
		Optional<MeetingPoint> checker = findByMeetingPointName(meetingPoint.getMeetingPointName());
		
		if (checker.isPresent()) {
			logger.info("[DAO] Meeting point with name: " + meetingPoint.getMeetingPointName() + " already exists ...");
			return false;
		}
		
		logger.info("[DAO] Meeting point with name: " + meetingPoint.getMeetingPointName() + " is available ...");
		
		// Attempting to insert new meeting point
		logger.info("[DAO] Attempting to insert new meeting point to DB ...");
		MeetingPoint insertedMeetingPoint = meetingPointRepository.save(meetingPoint);
		Integer id = insertedMeetingPoint.getId();
		boolean flag = meetingPointRepository.existsById(id);
		
		// Check if meeting point successfully inserted
		if (flag) {
			logger.info("[DAO] Insert new meeting point potentially successful ...");
			return true;
		}
		logger.info("[DAO] Insert new meeting point fail ...");
		return false;
	}

	@Override
	public boolean updateMeetingPoint(MeetingPoint meetingPoint) {
		logger.info("[DAO] Update Meeting Point DAO triggered!");
		
		// Check if meeting point exist
		boolean flag = meetingPointRepository.existsById(meetingPoint.getId());
		if (!flag) {
			logger.info("[DAO] Meeting point doesn't exist ...");
			return false;
		}
		logger.info("[DAO] Meeting point with ID: " + meetingPoint.getId() + " exist ...");
		
		// Updating meeting point
		meetingPointRepository.save(meetingPoint);
		
		logger.info("[DAO] Updating Meeting Point potentially successful ...");
		return true;
	}

	@Override
	public boolean deleteMeetingPoint(Integer id) {
		logger.info("[DAO] Delete Meeting Point DAO triggered!");
		meetingPointRepository.deleteById(id);
		return true;
	}

}

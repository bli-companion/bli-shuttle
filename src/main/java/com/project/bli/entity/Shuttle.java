package com.project.bli.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name="shuttle")
public class Shuttle {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SHUTTLE_SEQ")
	@SequenceGenerator(sequenceName="shuttle_seq", allocationSize=1, name="SHUTTLE_SEQ")
	private Integer id;
	
	@NotNull
	@Size(max = 40)
	@Column(name="name")
	private String name;
	
	@NotNull
	@Column(name="capacity")
	private Integer capacity;
	
	@NotNull
	@Size(max = 20)
	@Column(name="size_category")
	private String sizeCategory;
	
	@ManyToOne(optional=false)
	@JoinColumn(name="meeting_point_id", nullable=false)
	@OnDelete(action=OnDeleteAction.CASCADE)
	private MeetingPoint meetingPoint;
	
	public Shuttle() { }
	
	public Shuttle(String name, Integer capacity, String sizeCategory) {
		this.name = name;
		this.capacity = capacity;
		this.sizeCategory = sizeCategory;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public String getSizeCategory() {
		return sizeCategory;
	}

	public void setSizeCategory(String sizeCategory) {
		this.sizeCategory = sizeCategory;
	}

	public MeetingPoint getMeetingPoint() {
		return meetingPoint;
	}

	public void setMeetingPoint(MeetingPoint meetingPoint) {
		this.meetingPoint = meetingPoint;
	}

	@Override
	public String toString() {
		return "Shuttle [id=" + id + ", name=" + name + ", capacity=" + capacity + ", sizeCategory=" + sizeCategory
				+ ", meetingPoint=" + meetingPoint + "]";
	}
	
}

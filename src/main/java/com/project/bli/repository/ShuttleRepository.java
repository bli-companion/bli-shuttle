package com.project.bli.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.bli.entity.Shuttle;

@Repository
public interface ShuttleRepository extends JpaRepository<Shuttle, Integer> {

	Optional<Shuttle> findByName(String name);
	List<Shuttle> findBySizeCategory(String size);
	
}

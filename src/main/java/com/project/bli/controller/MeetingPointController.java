package com.project.bli.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.bli.entity.MeetingPoint;
import com.project.bli.service.impl.MeetingPointServiceImpl;

@RestController
@RequestMapping("/api/shuttle/mp")
public class MeetingPointController {

	private static final Logger logger = LoggerFactory.getLogger(MeetingPointController.class);
	
	@Autowired
	private MeetingPointServiceImpl meetingPointService;
	
	@GetMapping
	public ResponseEntity<List<MeetingPoint>> findAllMeetingPoint() {
		logger.info("[CONTROLLER] Find all meeting point controller triggered!");
		return new ResponseEntity<List<MeetingPoint>>(meetingPointService.findAllMeetingPoint(), HttpStatus.OK);
	}
	
	@GetMapping("/name/{name}")
	public ResponseEntity<Optional<MeetingPoint>> findByMeetingPointName(@PathVariable String name) {
		logger.info("[CONTROLLER] Find meeting point by name controller triggered!");
		Optional<MeetingPoint> meetingPoint = meetingPointService.findByMeetingPointName(name);
		return new ResponseEntity<Optional<MeetingPoint>>(meetingPoint, HttpStatus.OK);
	}
	
	@GetMapping("/id/{id}")
	public ResponseEntity<Optional<MeetingPoint>> findById(@PathVariable Integer id) {
		logger.info("[CONTROLLER] Find meeting point by ID controller triggered!");
		Optional<MeetingPoint> meetingPoint = meetingPointService.findById(id);
		return new ResponseEntity<Optional<MeetingPoint>>(meetingPoint, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Void> createMeetingPoint(@Valid @RequestBody MeetingPoint meetingPoint) {
		logger.info("[CONTROLLER] Create new Meeting Point controller triggered!");
		boolean flag = meetingPointService.createMeetingPoint(meetingPoint);
		
		if (flag) {
			logger.info("[CONTROLLER] Successfully created new Meeting Point ...");
			return new ResponseEntity<Void>(HttpStatus.CREATED);
		}
		logger.info("[CONTROLLER] Failed to create new Meeting Point ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
	@PutMapping
	public ResponseEntity<Void> updateMeetingPoint(@Valid @RequestBody MeetingPoint meetingPoint) {
		logger.info("[CONTROLLER] Update Meeting Point controller triggered!");
		boolean flag = meetingPointService.updateMeetingPoint(meetingPoint);
		
		if (flag) {
			logger.info("[CONTROLLER] Successfully updating existing Meeting Point ...");
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		logger.info("[CONTROLLER] Failed to update existing Meeting Point ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Void> deleteMeetingPoint(@PathVariable Integer id) {
		logger.info("[CONTROLLER] Delete Meeting Point controller triggered!");
		boolean flag = meetingPointService.deleteMeetingPoint(id);
		
		if (flag) {
			logger.info("[CONTROLLER] Successfully deleting existing Meeting Point ...");
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		logger.info("[CONTROLLER] Failed to delete existing Meeting Point ...");
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	}
	
}

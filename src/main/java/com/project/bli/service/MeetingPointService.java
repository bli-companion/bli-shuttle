package com.project.bli.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.project.bli.entity.MeetingPoint;

public interface MeetingPointService {

	List<MeetingPoint> findAllMeetingPoint();
	Optional<MeetingPoint> findByMeetingPointName(String name);
	List<MeetingPoint> findByTimeToBli(Date timeToBli);
	List<MeetingPoint> findByTimeFromBli(Date timeFromBli);
	Optional<MeetingPoint> findById(Integer id);
	
	boolean createMeetingPoint(MeetingPoint meetingPoint);
	boolean updateMeetingPoint(MeetingPoint meetingPoint);
	boolean deleteMeetingPoint(Integer id);
	
}

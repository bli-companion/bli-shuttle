package com.project.bli.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.bli.entity.MeetingPoint;

@Repository
public interface MeetingPointRepository extends JpaRepository<MeetingPoint, Integer> {

	Optional<MeetingPoint> findByMeetingPointName(String name);
	List<MeetingPoint> findByTimeToBli(Date time_to_bli);
	List<MeetingPoint> findByTimeFromBli(Date time_from_bli);
	
}

package com.project.bli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BliShuttleServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BliShuttleServiceApplication.class, args);
	}
}

package com.project.bli.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="meeting_point")
public class MeetingPoint {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="MEETING_POINT_SEQ")
	@SequenceGenerator(sequenceName="meeting_point_seq", allocationSize=1, name="MEETING_POINT_SEQ")
	private Integer id;
	
	@NotNull
	@Size(max=100)
	@Column(name = "meeting_point_name")
	private String meetingPointName;
	
	@NotNull
	@Column(name = "time_to_bli")
	private Date timeToBli;
	
	@NotNull
	@Column(name = "time_from_bli")
	private Date timeFromBli;
	
	@NotNull
	@Size(max = 40)
	@Column(name = "pic_name")
	private String picName;
	
	@NotNull
	@Size(max = 14)
	@Column(name = "pic_phone_number")
	private String picPhoneNumber;
	
	public MeetingPoint() {	}
	
	public MeetingPoint(String meetingPointName, Date timeToBli, 
		Date timeFromBli, String picName, String picPhoneNumber) {
			this.meetingPointName = meetingPointName;
			this.timeToBli = timeToBli;
			this.timeFromBli = timeFromBli;
			this.picName = picName;
			this.picPhoneNumber = picPhoneNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getMeetingPointName() {
		return meetingPointName;
	}

	public void setMeetingPointName(String meetingPointName) {
		this.meetingPointName = meetingPointName;
	}

	public Date getTimeToBli() {
		return timeToBli;
	}

	public void setTimeToBli(Date timeToBli) {
		this.timeToBli = timeToBli;
	}

	public Date getTimeFromBli() {
		return timeFromBli;
	}

	public void setTimeFromBli(Date timeFromBli) {
		this.timeFromBli = timeFromBli;
	}

	public String getPicName() {
		return picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getPicPhoneNumber() {
		return picPhoneNumber;
	}

	public void setPicPhoneNumber(String picPhoneNumber) {
		this.picPhoneNumber = picPhoneNumber;
	}

	@Override
	public String toString() {
		return "MeetingPoint [id=" + id + ", meetingPointName=" + meetingPointName + ", timeToBli=" + timeToBli
				+ ", timeFromBli=" + timeFromBli + ", picName=" + picName + ", picPhoneNumber=" + picPhoneNumber + "]";
	}
	
}
